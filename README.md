Hello, thank you for viewing my project!

First, some housecleaning: Contact me at either (646) 675-5625 via text or at jmkim.nyc@gmail.com

Secondly, once you've cloned my repo, run in your terminal 'npm install --save ejs express, morgan, debug, favicon, bodyparser, cookieparser'

Then, run 'node app.js'

Enjoy!
