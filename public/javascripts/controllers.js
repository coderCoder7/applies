var app = angular.module('jobsApp', []);

app.controller('jobsCtrl', [
  '$scope',
  '$http',
  function($scope, $http) {
    $scope.applicants = [];
    $scope.jobs = [];
    $scope.skills = [];
    $http.get('data.json').then(function(response) {
      $scope.applicants = response.data.applicants;
      $scope.jobs = response.data.jobs;
      $scope.skills = response.data.skills;
    });
  },
]);
